package DAO;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JTable;

public class TableGenerator<T> {
	protected static final Logger LOGGER = Logger.getLogger(TableGenerator.class.getName());
	@SuppressWarnings("unused")
	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public TableGenerator() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	private ArrayList<String> retrieveFields(T t) {
		ArrayList<String> list = new ArrayList<String>();
		for (Field field : t.getClass().getDeclaredFields()) {
			list.add(field.getName());
		}
		return list;
	}
	
	private ArrayList<Object> retrieveFieldValues(T t) {
		ArrayList<Object> list = new ArrayList<Object>();
		for (Field field : t.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				Object value;
				try {
					value = field.get(t);
					list.add(value);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
		}
		return list;
	}
	
	public String[][] buildDataMatrix(List<T> objects, int nrColumns) {
		if (objects.size() > 0 && nrColumns > 0) {
			String[][] data = new String[objects.size()][nrColumns];
			int line = 0;
			int column = 0;
			for (T iterator : objects) {
				ArrayList<Object> fieldValues = retrieveFieldValues(iterator);
				for(column=0;column<fieldValues.size();column++){
					data[line][column] = fieldValues.get(column).toString();
				}
				line++;
			}
			return data;
		}
		return null;
	}

	public JTable createTable(List<T> objects) {
		if (objects.size() > 0) {
			ArrayList<String> fields = retrieveFields(objects.get(0));
			int nrColumns = fields.size();
			String[] columns = new String[nrColumns];
			for (int i = 0; i < nrColumns; i++) {
				columns[i] = fields.get(i);
			}

			String[][] data = buildDataMatrix(objects, nrColumns);
			JTable table = new JTable(data, columns);
			return table;
		}
		return null;
	}
}
